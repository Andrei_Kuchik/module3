﻿using NUnit.Framework;

namespace StringParser.Test
{
	[TestFixture]
	public class StringParserTests
	{
		[TestCase("123", 123)]
		[TestCase("8493", 8493)]
		[TestCase("8282549", 8282549)]
		[TestCase("-89253", -89253)]
		[TestCase("2147483647", 2147483647)]
		public void TestMethod1(string actual, int excepted)
		{
			var convertActual = StringParser.ParseToInt(actual);
			Assert.AreEqual(convertActual, excepted);
		}
	}
}