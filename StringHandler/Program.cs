﻿using System;

namespace StringHandler
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			while (true)
			{
				var value = Console.ReadLine();
				try
				{
					Console.WriteLine(StringReader.GetFirstLetter(value));
				}
				catch
				{
					Console.WriteLine("Input string not should to be empty or null!");
				}
			}
		}
	}
}