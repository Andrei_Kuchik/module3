﻿using System;
using System.Linq;

namespace StringHandler
{
	public static class StringReader
	{
		public static string GetFirstLetter(string enterString)
		{
			if (string.IsNullOrWhiteSpace(enterString))
			{
				throw new ArgumentException();
			}

			return enterString.First().ToString();
		}
	}
}