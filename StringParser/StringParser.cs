﻿using System;
using System.Linq;
using System.Text;

namespace StringParser
{
	public static class StringParser
	{
		public static int ParseToInt(string parseString)
		{
			if (string.IsNullOrWhiteSpace(parseString))
			{
				throw new ArgumentNullException();
			}

			var asciiBytes = Encoding.ASCII.GetBytes(parseString);

			CheckArrayOfBytes(asciiBytes);

			var returnIntValue = 0;
			Array.Reverse(asciiBytes);
			var counter = 1;
			asciiBytes.ToList().ForEach(x =>
			{
				if (x != 45)
				{
					returnIntValue += (Convert.ToChar(x) - 48) * counter;
					counter *= 10;
				}
				else
				{
					returnIntValue = returnIntValue * -1;
				}
			});

			return returnIntValue;
		}

		private static void CheckArrayOfBytes(byte[] bytesArray)
		{
			foreach (var element in bytesArray)
			{
				if (element >= 48 && element <= 57 || element == 45)
				{
					if (element == 45 && element != bytesArray.First())
					{
						throw new FormatException();
					}
				}
				else
				{
					throw new FormatException();
				}
			}
		}
	}
}